export const themeColors = {
  bgcolor: '#f8f8f8',
  primary: '#cfa05e',
  secondary: '#059669',
  accent: '#9333ea',
  neutral: '#d6d3d1',
  info: '#60a5fa',
  success: '#4ade80',
  warning: '#fbbf24',
  error: '#fb7185',
  overlay: 'rgba(0,0,0, 0.4)'
}
