import { describe, expect, it, beforeEach, vi } from 'vitest'
import { mount } from '@vue/test-utils'
import UiModal from '@/shared/ui/UiModal/ui/UiModal.vue'
import { createTestingPinia } from '@pinia/testing'
import { useModalStore } from '@/shared/ui/UiModal/model/store/modal'
import { h } from 'vue'
import UiOverlay from '@/shared/ui/UiOverlay/UiOverlay.vue'

const testPinia = createTestingPinia({
  createSpy: vi.fn,
  stubActions: false,
  initialState: {
    modal: {
      isOpen: true,
      modalData: h('div', {class:'modal-test'}, 'some test text')
    }
  }
})

describe('UiModal', () => {

  it('Modal rendered', async () => {
    const wrapper = mount(UiModal, {
      global: {
        plugins: [testPinia],
      },
    })

    const store = useModalStore()

    expect(wrapper.exists()).toBe(true)
    const overlay = wrapper.getComponent(UiOverlay)
    expect(overlay.find('.modal-test').text()).toBe('some test text')

  })

  it('should close modal on overlay click', async () => {
    const wrapper = mount(UiModal, {
      global: {
        plugins: [testPinia],
      },
    })
    const overlay = wrapper.getComponent(UiOverlay)
    expect(overlay.find('.modal-test').text()).toBe('some test text')
    await overlay.trigger('click')
    expect(wrapper.html()).toBe('<!--v-if-->')
  })
})