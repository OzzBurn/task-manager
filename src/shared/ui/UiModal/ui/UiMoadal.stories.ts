import type { Meta, StoryObj } from '@storybook/vue3'
import UiModal from '@/shared/ui/UiModal/ui/UiModal.vue'
import { useModalStore } from '@/shared/ui/UiModal/model/store/modal'
import { type Component, h } from 'vue'

const mockComponent: Component = h('div', {
  class: 'bg-bgcolor p-5 rounded-md'
}, 'some modal content')

const meta = {
  title: 'Ui-Kit/UiModal',
  component: UiModal,

  parameters: {
    layout: 'fullscreen centered',
  },

  tags: ['autodocs'],

  render: () => ({
    components: {UiModal},
    template: `<UiModal />`
  }),

  decorators: [(story) => ({
    created() {
      this.modal = useModalStore()
    },
    data: () => ({
      modal: null
    }),
    methods: {
      handleClick() {
        this.modal.openModal(mockComponent)
      }
    },
    template: `
      <div class="flex justify-center items-center p-5">
        <button class="py-2 px-3 bg-primary rounded-md" @click="handleClick">open modal</button>
        <story/>  
      </div>
      
    `
  })]

} satisfies Meta<typeof UiModal>

export default meta

type Story = StoryObj<typeof meta>

export const ModalExample: Story = {}