import { defineStore } from 'pinia'
import type { ITask, ITaskEditInput, ITaskInput } from './types'
import { ref } from 'vue'
import { createTask, editTask, getAllTasks } from '@/entities/Task/model/api'

export const useTaskStore = defineStore('tasks', () => {
  const taskList = ref<ITask[]>([])

  const getTaskById = (id: string): ITask | undefined => taskList.value.find((task) => task.id === id)

  const fetchTasks = async () => {
    try {
      const tasks = await getAllTasks()
      taskList.value = tasks

      return tasks
    } catch (error) {
      //error
    }
  }

  const getTasksByStatusName = (statusName:string):ITask[] => {
    return taskList.value.filter(task => task.status === statusName)
  }

  const addTask = async (task: ITaskInput) => {
    const newTask = await createTask(task)

    taskList.value.push(newTask)
  }

  const updateTask = async (id: string, task: ITaskEditInput) => {
    const newTask: ITask = await editTask(id, task)
    const taskIndex = taskList.value.findIndex((task) => task.id === newTask.id)

    taskList.value.splice(taskIndex, 1, newTask)
  }

  return {
    taskList,
    getTasksByStatusName,
    getTaskById,
    fetchTasks,
    addTask,
    updateTask,
  }
})
