import { useDnDStore } from '@/shared/helpers/DragAndDrop/DnDStore'
import { storeToRefs } from 'pinia'


export const useDnD = () => {
  const DnDStore = useDnDStore()
  const {dragData} = storeToRefs(DnDStore)
  const vDraggable = {
    mounted: (el, data) => {
      el.setAttribute('draggable', 'true');
      el.addEventListener('dragstart', () => {
        DnDStore.setDragData(data.value)
      })
    }
  }

  const vDropZone = {
    mounted: (el, data) => {

      el.addEventListener('dragover', (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = "move";
      })

      el.addEventListener('drop', (event) => {
        event.preventDefault();
        data.value(dragData.value)
      })
    }
  }

  return {
    vDraggable,
    vDropZone
  }
}