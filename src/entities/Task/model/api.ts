import { http } from '@/shared'
import type { ITask, ITaskEditInput, ITaskInput } from '@/entities/Task/model/types'

export const getAllTasks = async (): Promise<ITask[]> => {
  const {data: {data: {tasks}}} = await http({
    method: 'POST',
    data: {
      query: `{
        tasks {
          id
          title
          description
          status
        }
      }`,
    }
  })

  return tasks
}

export const createTask = async (task: ITaskInput): Promise<ITask> => {
  const createTask = `
      mutation createTask($title: String!, $description: String!, $status: String!) {
        createTask(createTaskInput: {title: $title, description: $description, status: $status}) {
          id
          title
          description
          status
        }   
      }`
  const {data: {data: {createTask: newTask}}} = await http({
    method: 'POST',
    data: {
      query: createTask,
      variables: task
    }
  })

  return newTask
}

export const editTask = async (id: string, task: ITaskEditInput): Promise<ITask> => {
  const updateTask = `
      mutation ($id: String!, $updateTaskInput: UpdateTaskInput!) {
        updateTask(id: $id, updateTaskInput: $updateTaskInput) {
          id
          title
          description
          status
        }   
      }`
  const {data: {data: {updateTask: updatedTask}}} = await http({
    method: 'POST',
    data: {
      query: updateTask,
      variables: {
        id,
        updateTaskInput: task
      }
    }
  })

  return updatedTask
}