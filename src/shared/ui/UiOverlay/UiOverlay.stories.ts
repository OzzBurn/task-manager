import type { Meta, StoryObj } from '@storybook/vue3'
import UiOverlay from '@/shared/ui/UiOverlay/UiOverlay.vue'

const meta = {
  title: 'Ui-Kit/UiOverlay',
  component: UiOverlay,
  parameters: {
    layout: 'fullscreen',
  },

  tags: ['autodocs'],

} satisfies Meta<typeof UiOverlay>

export default meta

type Story = StoryObj<typeof meta>

export const modalView: Story = {}

