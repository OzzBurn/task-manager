export interface IStatusGroup {
  id?: number
  name: string
  title: string
}

export interface IProps {
  group: IStatusGroup
}