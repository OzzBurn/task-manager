import { describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'
import UiCard from '@/shared/ui/Container/UiCard/UiCard.vue'


describe('UiCard tests', () => {
  const actionSlotMark = 'div.flex.justify-end.gap-3'
  it('rendered empty card', () => {
    const component = mount(UiCard)

    expect(component.exists()).toBe(true)
  })

  it('rendered card without actions', () => {
    const component = mount(UiCard, {
      slots: {
        title: 'Title',
        content: `<div class="content-container">test content</div>`
      }
    })

    expect(component.find('h3').text()).toBe('Title')
    expect(component.find('.content-container').exists()).toBe(true)
    expect(component.find('.content-container').text()).toBe('test content')
    expect(component.find(actionSlotMark).exists()).toBe(false)
  })

  it('rendered card with actions', () => {
    const component = mount(UiCard, {
      slots: {
        actions: `<button>button text</button>`
      }
    })
    expect(component.find(actionSlotMark).exists()).toBe(true)
    expect(component.find(actionSlotMark).get('button').text()).toBe('button text')
  })
})