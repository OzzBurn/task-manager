export interface ITask {
  id?: string
  title: string
  description: string
  status: string
}

export interface IProps {
  task: ITask
}

export interface ITaskInput {
  title: string
  description: string
  status: string
}

export interface ITaskEditInput {
  title?: string
  description?: string
  status?: string
}
