import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { IStatusGroup } from '@/entities/StatusGroup/model/types'


export const useStatusGroups = defineStore('status-groups', () => {
  const list = ref<IStatusGroup[]>([])

    const fetchStatusGroups = async (): Promise<IStatusGroup[]> => {
    try {
      const result = await new Promise((resolve) => {
        resolve([
          {
            id: 1,
            name: 'created',
            title: 'Создана'
          },
          {
            id: 2,
            name: 'inWork',
            title: 'В работе'
          },
          {
            id: 3,
            name: 'complete',
            title: 'Выполнена'
          }
        ])
      })

      list.value = result
      return result
    } catch (err) {
      console.log(err)
    }
  }

  return {
    list,
    fetchStatusGroups
  }
})