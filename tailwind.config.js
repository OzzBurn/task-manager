/** @type {import('tailwindcss').Config} */
import { themeColors } from './src/shared/'

export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    colors: themeColors,
    extend: {}
  },
  plugins: []
}
