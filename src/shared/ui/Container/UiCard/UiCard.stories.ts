import {h} from 'vue'
import type { Meta, StoryObj } from '@storybook/vue3'
import UiCard from '@/shared/ui/Container/UiCard/UiCard.vue'

const meta = {
  title: 'Ui-Kit/UiCard',
  component: UiCard,

  parameters: {
    layout: 'centered',
    slots: {
      title: {
        description: 'title slot',
        template: '{{ args.title }}'
      },
      content: {
        description: 'content body slot',
        template: '{{ args.content }}'
      },
      actions: {
        description: 'actions slot',
        template: '{{ args.actions }}'
      }
    },
  },

  tags: ['autodocs'],

} satisfies Meta<typeof UiCard>

const actionsExample = () => {
  return [
    h('button', 'apply'),
    h('button', 'cancel')
  ]
}

export default meta

type Story = StoryObj<typeof meta>

export const EmptyCard: Story = {}

export const CardWithTitle: Story = {
  args: {
    title: 'Card Title'
  }
}

export const CardWithContent: Story = {
  args: {
    content: 'Card content'
  }
}

export const CardWithActions: Story = {
  args: {
    actions: actionsExample
  }
}

export const FullFilledCard: Story = {
  args: {
    title: 'Card Title',
    content: 'Some card content body',
    actions: actionsExample
  },
}