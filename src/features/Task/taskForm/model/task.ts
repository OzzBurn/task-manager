import type { ITask } from '@/entities/Task'
import { useTaskStore } from '@/entities/Task'

export const useTaskHandler = () => {
  const taskStore = useTaskStore()

  const addTask = (task: ITask) => {
    try {
      taskStore.addTask(task)
      //task add request
      //det new task into task list
    } catch (error) {
      //error
    }
  }

  return {
    addTask
  }
}
