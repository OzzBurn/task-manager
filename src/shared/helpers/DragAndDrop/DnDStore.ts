import { defineStore } from 'pinia'
import { ref } from 'vue'


export const useDnDStore = defineStore('dnd', () => {
  const dragData = ref<T>(null)

  const setDragData = (data: T):void => {
    dragData.value = data
  }

  return {
    dragData,
    setDragData
  }
})