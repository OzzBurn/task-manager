import { beforeEach, describe, expect, it } from 'vitest'
import { createPinia, setActivePinia } from 'pinia'
import { useModalStore } from '@/shared/ui/UiModal/model/store/modal'

const mockVueComponent = {
  tag: 'div',
  render() {}
}

describe('Modal Store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  it('sets isOpen', () => {
    const modal = useModalStore()
    expect(modal.isOpen).toBe(false)
    modal.openModal(mockVueComponent)
    expect(modal.isOpen).toBe(true)
    modal.closeModal()
    expect(modal.isOpen).toBe(false)
  })

  it('sets modalData', () => {
    const modal = useModalStore()
    expect(modal.modalData).toBe(null)
    modal.openModal(mockVueComponent)
    expect(modal.modalData).toStrictEqual(mockVueComponent)
    modal.closeModal()
    expect(modal.modalData).toBe(null)
  })
})