import { describe, expect, it } from 'vitest'
import UiOverlay from '@/shared/ui/UiOverlay/UiOverlay.vue'
import { mount } from '@vue/test-utils'

describe('Ui Overlay', () => {
  it('rendered', () => {
    const wrapper = mount(UiOverlay)

    expect(wrapper.exists()).toBe(true)
    expect(wrapper.find('div.fixed.w-full.h-full.bg-overlay').exists()).toBe(true)
  })
})