import {defineStore} from 'pinia'
import { ref, shallowRef } from 'vue'

export const useModalStore = defineStore('modal', () => {
  const isOpen = ref<boolean>(false)

  const modalData = shallowRef< | null>(null)
  const propsData = ref<any>(null)

  const openModal = (component: T, props: T = {}) => { // TODO: ???
    try {
      if (typeof component.render  === 'function') {
        modalData.value = component
        propsData.value = props
        isOpen.value = true
      } else {
        throw new Error(`not a component: ${component}`)
      }
    } catch (e) {
      console.error(e)
    }

  }

  const closeModal = () => {
    isOpen.value = false
    modalData.value = null
  }

  return {
    isOpen,
    modalData,
    propsData,
    openModal,
    closeModal
  }
})