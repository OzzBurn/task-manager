import { nextTick, onMounted, ref, type Ref, watch } from 'vue'

export const useElemPos = (elem:Ref) => {

  const height = ref(0)
  const bottom = ref(0)
  const left = ref(0)
  const right = ref(0)
  const top = ref(0)
  const width = ref(0)
  const x = ref(0)
  const y = ref(0)

  const updatePos = () => {
    console.log(2)
    const rect = elem.value.getBoundingClientRect();

    height.value = rect.height
    bottom.value = rect.bottom
    left.value = rect.left
    right.value = rect.right
    top.value = rect.top
    width.value = rect.width
    x.value = rect.x
    y.value = rect.y

    console.log(height.value)
  }

  watch(elem, () => {
    updatePos()
  })

  return {
    height,
    bottom,
    left,
    right,
    top,
    width,
    x,
    y
  }
}