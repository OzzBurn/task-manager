import StatusGroup from './ui/StatusGroup.vue'
import { useStatusGroups } from './model/statusGroups'

export { StatusGroup, useStatusGroups }