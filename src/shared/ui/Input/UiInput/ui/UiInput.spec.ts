import { mount } from '@vue/test-utils'
import { describe, expect, it } from 'vitest'
import UiInput from './UiInput.vue'

describe('UiInput', () => {
  it('rendered with label and text', async () => {
    const wrapper = mount(UiInput, {
      props: {
        label: 'test',
        modelValue: 'test text',
        'onUpdate:modelValue': (e: string) => wrapper.setProps({ modelValue: e })
      }
    })

    expect(wrapper.find('span').text()).toBe('test')
    expect(wrapper.props('modelValue')).toBe('test text')
    await wrapper.find('input').setValue('another text')
    expect(wrapper.props('modelValue')).toBe('another text')
  })

  it('changed label style on active and input value', async () => {
    const wrapper = mount(UiInput, {
      props: {
        label: 'test',
      }
    })

    const input = wrapper.find('input')

    expect(wrapper.find('span').classes('-translate-y-[125%]')).toBe(false)
    await input.setValue('test text')
    expect(input.element.value).toBe('test text')
    expect(wrapper.find('span').classes('-translate-y-[125%]')).toBe(true)
    await input.setValue('')
    expect(input.element.value).toBe('')
    expect(wrapper.find('span').classes('-translate-y-[125%]')).toBe(false)
  })
})
